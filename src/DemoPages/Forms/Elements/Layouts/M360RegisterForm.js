import React, {Fragment} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {
    Col, Row, Card, CardBody,
    CardTitle, Button, Form, FormGroup, Label, Input,Progress, FormText
} from 'reactstrap';


export default class FormGridFormRow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            client: '',
            email: '',
            address: '',
            city: '',
            state: '',
            cep: '',
            progressBar: '',
            progressBarLabel: '',
            progressBarValue: 10,
            progressBarColor: 'info',
            m360Button:''
        };
    }

    mySubmitHandler = async (event) => {
        event.preventDefault();
        const scope = this
        var url = "http://localhost:22870/create-environment";
        var method = "POST";
        var shouldBeAsync = true;
        
        var postData = {
            'clientName':scope.state.client,
            'address':scope.state.address,
            'state':scope.state.state,
            'city':scope.state.city,
            'cep':scope.state.cep,
            'phone':scope.state.phone,
            'cpfCnpj':scope.state.cpfCnpj,
            'email':scope.state.email
        };

        var request = new XMLHttpRequest();

        request.onload = function () {
            
            var status = request.status;

            if(status == 202){
                scope.setState({['progressBarLabel']: <Label for="progress-bar" id="label-progress-bar">Conta criada com sucesso, você ja pode acessar o M360, criar campanhas e disparar emails para seus clientes </Label>});
                scope.setState({['progressBarColor']: 'success'});
                scope.setState({['progressBarValue']: 100});
                scope.setState({['m360Button']: <Button color="primary" className="mt-2" onClick={scope.goToM360} id="m360-button">Acesse o M360</Button>});
                
            } else {
                scope.setState({['progressBarLabel']: <Label for="progress-bar" id="label-progress-bar">Erro ao criar a conta, tente novamente mais tarde</Label>});
                scope.setState({['progressBarColor']: 'danger'});
                scope.setState({['progressBarValue']: 100});
            }
        }

        scope.setState({['progressBarLabel']: <Label for="progress-bar" id="label-progress-bar">Aguarde, estamos criando sua conta, isso pode levar alguns minutos</Label>});
        document.getElementById('progress-bar').style.visibility = 'visible';
        scope.disabledForm()

        request.open(method, url, shouldBeAsync);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify(postData));

    }

    disabledForm = () => {
        document.getElementById("client").disabled = true;
        document.getElementById("address").disabled = true;
        document.getElementById("state").disabled = true;
        document.getElementById("phone").disabled = true;
        document.getElementById("cpfCnpj").disabled = true;
        document.getElementById("city").disabled = true;
        document.getElementById("state").disabled = true;
        document.getElementById("cep").disabled = true;
        document.getElementById("submmitButton").disabled = true;
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }

    goToM360 = () => {
        window.location.href = "https://platform.dev.ucampaign.unear.net/"
    }

    render() {
        return (
            <Fragment>
                <ReactCSSTransitionGroup
                    component="div"
                    transitionName="TabsAnimation"
                    transitionAppear={true}
                    transitionAppearTimeout={0}
                    transitionEnter={false}
                    transitionLeave={false}>
                    <Card className="main-card mb-3">
                        <CardBody>
                            <CardTitle>Registro M360</CardTitle>
                            <Form onSubmit={this.mySubmitHandler} >
                                <FormGroup>
                                    <Label for="client">Organização</Label>
                                    <Input 
                                        type="text" 
                                        name="client" 
                                        id="client"
                                        placeholder="Nome da empresa" 
                                        onChange={this.myChangeHandler} required/>
                                </FormGroup>
                                <Row form>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="email">Email</Label>
                                            <Input type="email" name="email" id="email"
                                                   placeholder="Email Principal" onChange={this.myChangeHandler} min={10} max={200} required/>
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="phone">Telefone</Label>
                                            <Input type="tel" name="phone" id="phone"
                                                placeholder="11999999999" onChange={this.myChangeHandler} pattern="[0-9]{11}" required/>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row form>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="address">Endereço</Label>
                                            <Input type="text" name="address" id="address"
                                                placeholder="Endereço" onChange={this.myChangeHandler} min={5} max={300} required/>
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="cpfCnpj">CPF ou CNPJ</Label>
                                            <Input type="text" name="cpfCnpj" id="cpfCnpj"
                                                   placeholder="CPF ou CNPJ" onChange={this.myChangeHandler} required/>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row form>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="city">Cidade</Label>
                                            <Input placeholder="Cidade" type="text" name="city" id="city" onChange={this.myChangeHandler} min={2} max={200} required/>
                                        </FormGroup>
                                    </Col>
                                    <Col md={4}>
                                        <FormGroup>
                                            <Label for="state">Estado</Label>
                                            <Input placeholder="Estado" type="text" name="state" id="state" onChange={this.myChangeHandler} min={2} max={200} required/>
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <Label for="cep">CEP</Label>
                                            <Input placeholder="00000-000" type="text" name="cep" id="cep" onChange={this.myChangeHandler} min={8} max={8} required/>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Button color="primary" className="mt-2" id="submmitButton">Registrar</Button>
                            </Form><br/>
                            {this.state.progressBarLabel}
                            <Progress className="mb-3" animated color={this.state.progressBarColor} value={this.state.progressBarValue} name="progress-bar" id="progress-bar" style={{visibility: 'hidden'}}/>
                            {this.state.m360Button}
                        </CardBody>
                    </Card>
                </ReactCSSTransitionGroup>
            </Fragment>
        );
    }
}
