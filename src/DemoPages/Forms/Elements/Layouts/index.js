import React, {Fragment} from 'react'

import Tabs from 'react-responsive-tabs';

import PageTitle from '../../../../Layout/AppMain/PageTitle';

// Examples

import FormGrid from './Examples/FormGrid';
import M360RegisterForm from './M360RegisterForm';

const tabsContent = [
    {
        title: 'Layout ',
        content: <M360RegisterForm/>
    },
    // {
    //     title: 'Grid',
    //     content: <FormGrid/>
    // }

];

function getTabs() {
    return tabsContent.map((tab, index) => ({
        title: tab.title,
        getContent: () => tab.content,
        key: index,
    }));
}

class FormElementsLayouts extends React.Component {

    render() {
        return (
            <Fragment>
                <PageTitle
                    heading="M360 Campaign"
                    subheading="Registro M360"
                    icon="pe-7s-graph text-success"
                />
                <M360RegisterForm/>
                {/* <Tabs tabsWrapperClass="body-tabs body-tabs-layout" transform={false} showInkBar={true} items={getTabs()}/> */}
            </Fragment>
        )
    }
}

export default FormElementsLayouts;



